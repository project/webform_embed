<?php

namespace Drupal\webform_embed\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WebformEmbedController.
 */
class WebformEmbedController extends ControllerBase {

  /**
   * Entity Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * WebformEmbedController constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('entity_type.manager')
    );
  }

  /**
   * Display webform.
   *
   * @param mixed $webform
   *   Webform ID.
   *
   * @return array
   *   Render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function displayForm(mixed $webform): array {

    $form = $this->loadForm($webform);
    if ($form) {
      $output = $this->renderForm($form);
    }
    else {
      $output = $this->t('No webform to display.');
    }

    return [
        '#theme' => 'page__webform_embed',
        '#webform_output' => $output,
    ];

  }

  /**
   * Load the webform.
   *
   * @param mixed $machineName
   *   Machine name.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Webform entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadForm(mixed $machineName) {
    try {
      $form = $this->entityManager
          ->getStorage('webform')
          ->load($machineName);
      return $form;
    }
    catch (RequestException $e) {
      $message = $this->t('Webform failed to load :: @message', [
          '@message' => $e->getMessage(),
      ]);
      \Drupal::logger('webform_embed')->error($message);
    }
    return NULL;
  }

  /**
   * Render form.
   *
   * @param \Drupal\Core\Entity\EntityInterface $form
   *   Webform entity.
   *
   * @return array
   *   Render array.
   */
  protected function renderForm(EntityInterface $form) {
    $output = $this->entityManager
        ->getViewBuilder('webform')
        ->view($form);
    return $output;
  }

}
