<?php

namespace Drupal\webform_embed\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

/**
 * Class WebformEmbedSubscriber.
 */
class WebformEmbedSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events['kernel.response'] = ['kernelResponse'];

    return $events;
  }

  /**
   * This method is called whenever the `kernel.request` event is dispatched.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $responseEvent
   *   Response event.
   */
  public function kernelResponse(ResponseEvent $responseEvent): void {
    $current_path = \Drupal::service('path.current')->getPath();
    $patterns = "/webform_embed/displayForm/*\n/form/*";
    $match = \Drupal::service('path.matcher')->matchPath($current_path, $patterns);

    if ($match) {
      $response = $responseEvent->getResponse();
      $response->headers->remove('X-Frame-Options');
    }
  }

}
